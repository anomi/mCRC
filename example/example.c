#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "crc.h"
#include "crc_configs.h"

int main(int argc, char *argv[]) {
    CRC_Handle_t h;
    h.config = CRC8_AUTOSAR;

    const uint32_t dataLen = 9;
    const uint32_t width = h.config.width;
    const uint32_t crcLen = (width / 8);
    uint8_t sampleData[] = {0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x00, 0x00, 0x00, 0x00};
    uint32_t crc = 0;

    CRC_init(&h);

    CRC_Result_t r = CRC_calcBlock(&h, sampleData, dataLen);
    if (r.isValid) {
        crc = r.crc;
        CRC_attachCRC(&h, sampleData, dataLen + crcLen, crc);
    }

    if (CRC_checkBlock(&h, sampleData, dataLen + crcLen)) {
        printf("Success!\n");
    } else {
        printf("Failure!\n");
    }

    return 0;
}
