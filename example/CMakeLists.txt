project(example)

add_executable(${PROJECT_NAME}_c example.c)
target_link_libraries(${PROJECT_NAME}_c mCRC)

add_executable(${PROJECT_NAME}_cc example.cc)
target_link_libraries(${PROJECT_NAME}_cc mCRC)
