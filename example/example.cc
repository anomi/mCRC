#include "crc.hh"
#include "crc_configs.h"
#include <iostream>

using namespace std;
using namespace mcrc;

int main(int argc, char **argv) {
    const uint32_t dataLen = 9;
    uint8_t sampleData[] = {0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x00, 0x00, 0x00, 0x00};
    Crc crc = Crc(CRC16_ARC);
    result r = crc.calc(sampleData, dataLen);
    if (r.isValid) {
        crc.attach(sampleData, dataLen + crc.get().width / 8, r.crc);
    }
    if (crc.check(sampleData, dataLen + crc.get().width / 8)) {
        cout << "Success!" << "\n";
    } else {
        cout << "Failure!" << "\n";
    }
}

