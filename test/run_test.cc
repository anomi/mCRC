#include "gtest/gtest.h"
#include "crc.h"
#include "crc_configs.h"

using namespace mcrc;


uint8_t sampleData[] = {0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x00, 0x00, 0x00, 0x00};
const uint32_t dataLen = 9;

const int cfg8Len = 9;
CRC_Config_t cfg8[] = {CRC8_CRC8, CRC8_AUTOSAR, CRC8_BLUETOOTH,
                       CRC8_DARC, CRC8_EBU, CRC8_ICODE,
                       CRC8_MAXIM, CRC8_OPENSAFETY, CRC8_SAE_J1850};
const int cfg16Len = 23;
CRC_Config_t cfg16[] = {CRC16_ARC, CRC16_AUG_CCITT, CRC16_BUYPASS,
                        CRC16_CCITT_FALSE, CRC16_DDS_110, CRC16_DECT_R,
                        CRC16_DECT_X, CRC16_DNP, CRC16_EN_13757,
                        CRC16_GENIBUS, CRC16_MAXIM, CRC16_MCRF4XX,
                        CRC16_OPENSAFETY_A, CRC16_OPENSAFETY_B, CRC16_PROFIBUS,
                        CRC16_T10_DIF, CRC16_T10_TELEDISK, CRC16_TMS37157,
                        CRC16_CRC_A, CRC16_CCITT_TRUE, CRC16_MODBUS,
                        CRC16_X_25, CRC16_XMODEM};
const int cfg32Len = 10;
CRC_Config_t cfg32[] = {CRC32_CRC32, CRC32_AUTOSAR, CRC32_BZIP2,
                        CRC32_32C, CRC32_32D, CRC32_MPEG_2,
                        CRC32_POSIX, CRC32_32Q, CRC32_JAMCRC,
                        CRC32_XFER};

class CrcTests : public testing::Test {
protected:
    virtual void SetUp() {

        for (int i = 0; i < cfg8Len; ++i) {
            h8[i].config = cfg8[i];
            CRC_init(&h8[i]);
        }
        for (int i = 0; i < cfg16Len; ++i) {
            h16[i].config = cfg16[i];
            CRC_init(&h16[i]);
        }
        for (int i = 0; i < cfg32Len; ++i) {
            h32[i].config = cfg32[i];
            CRC_init(&h32[i]);
        }

        for (int i = 0; i < 9; ++i) {
            sampleData[i] = 0x31 + i;
        }
        for (int i = 0; i < 4; ++i) {
            sampleData[dataLen+i] = 0x00;
        }
    }

    virtual void TearDown() { }

    CRC_Handle_t h8[cfg8Len];
    CRC_Handle_t h16[cfg16Len];
    CRC_Handle_t h32[cfg32Len];
};


TEST(CRC_init, Positiv) {
    CRC_Handle_t h;
    h.config = CRC16_ARC;
    EXPECT_TRUE(CRC_init(&h));
    h.config = CRC8_AUTOSAR;
    EXPECT_TRUE(CRC_init(&h));
    h.config = CRC32_32C;
    EXPECT_TRUE(CRC_init(&h));
}

TEST(CRC_init, Negative) {
    CRC_Handle_t h;
    h.config =  CRC16_X_25;
    EXPECT_FALSE(CRC_init(NULL));

    h.config =  CRC16_X_25;
    h.config.width = 7;
    EXPECT_FALSE(CRC_init(&h));

    h.config =  CRC16_X_25;
    h.config.width = 64;
    EXPECT_FALSE(CRC_init(&h));

    h.config =  CRC16_X_25;
    h.config.refIn = false;
    EXPECT_FALSE(CRC_init(&h));

    h.config = CRC16_X_25;
    h.config.poly = 0x1FFFF;
    EXPECT_FALSE(CRC_init(&h));
    h.config = CRC8_CRC8;
    h.config.poly = 0x1FF;
    EXPECT_FALSE(CRC_init(&h));

    h.config = CRC16_X_25;
    h.config.xorOut = 0x1FFFF;
    EXPECT_FALSE(CRC_init(&h));
    h.config = CRC8_CRC8;
    h.config.xorOut = 0x1FF;
    EXPECT_FALSE(CRC_init(&h));

    h.config = CRC16_X_25;
    h.config.init = 0x1FFFF;
    EXPECT_FALSE(CRC_init(&h));
    h.config = CRC8_CRC8;
    h.config.init = 0x1FF;
    EXPECT_FALSE(CRC_init(&h));
}

TEST_F(CrcTests, CalcBlock8Positiv) {
    for (auto h : h8) {
        CRC_Result_t r = CRC_calcBlock(&h, sampleData, dataLen);
        EXPECT_TRUE(r.isValid);
        EXPECT_GT(r.crc,0);
    }
}

TEST_F(CrcTests, CalcBlock16Positiv) {
    for (auto h : h16) {
        CRC_Result_t r = CRC_calcBlock(&h, sampleData, dataLen);
        EXPECT_TRUE(r.isValid);
        EXPECT_GT(r.crc,0);
    }
}

TEST_F(CrcTests, CalcBlock32Positiv) {
    for (auto h : h32) {
        CRC_Result_t r = CRC_calcBlock(&h, sampleData, dataLen);
        EXPECT_TRUE(r.isValid);
        EXPECT_GT(r.crc,0);
    }
}

TEST_F(CrcTests, CalcBlockNegative) {
    CRC_Handle_t h;
    CRC_Result_t r = CRC_calcBlock(NULL, sampleData, dataLen);
    EXPECT_FALSE(r.isValid);

    r = CRC_calcBlock(&h, NULL, dataLen);
    EXPECT_FALSE(r.isValid);

    r = CRC_calcBlock(&h, sampleData, 0);
    EXPECT_FALSE(r.isValid);
}

TEST_F(CrcTests, CalcByte8Positive) {
    for (auto h : h8) {
        CRC_Result_t r = CRC_calcByte(&h, 0x1, true);
        EXPECT_TRUE(r.isValid);
        EXPECT_GT(r.crc, 0);

        for (int i = 0; i < dataLen-1; ++i) {
            CRC_calcByte(&h, sampleData[i], false);
        }
        r = CRC_calcByte(&h, sampleData[8], true);
        EXPECT_TRUE(r.isValid);
        EXPECT_GT(r.crc, 0);
    }
}

TEST_F(CrcTests, CalcByte16Positive) {
    for (auto h : h16) {
        CRC_Result_t r = CRC_calcByte(&h, 0x1, true);
        EXPECT_TRUE(r.isValid);
        EXPECT_GT(r.crc, 0);

        for (int i = 0; i < dataLen-1; ++i) {
            CRC_calcByte(&h, sampleData[i], false);
        }
        r = CRC_calcByte(&h, sampleData[8], true);
        EXPECT_TRUE(r.isValid);
        EXPECT_GT(r.crc, 0);
    }
}

TEST_F(CrcTests, CalcByte32Positive) {
    for (auto h : h32) {
        CRC_Result_t r = CRC_calcByte(&h, 0x1, true);
        EXPECT_TRUE(r.isValid);
        EXPECT_GT(r.crc, 0);

        for (int i = 0; i < dataLen-1; ++i) {
            CRC_calcByte(&h, sampleData[i], false);
        }
        r = CRC_calcByte(&h, sampleData[8], true);
        EXPECT_TRUE(r.isValid);
        EXPECT_GT(r.crc, 0);
    }
}

TEST_F(CrcTests, CalcByteNegative) {
    EXPECT_FALSE(CRC_calcByte(NULL, 0x01, true).isValid);
}

TEST_F(CrcTests, Attach8Positive) {
    int bytes = 1;
    for (auto h : h8) {
        EXPECT_TRUE(CRC_attachCRC(&h, sampleData, dataLen + bytes, UINT32_MAX));
        for (int i = 0; i < bytes; ++i) {
            EXPECT_EQ(sampleData[dataLen+i], 0xFF);
            sampleData[dataLen+i] = 0x00;
        }
    }
}

TEST_F(CrcTests, Attach16Positive) {
    int bytes = 2;
    for (auto h : h16) {
        EXPECT_TRUE(CRC_attachCRC(&h, sampleData, dataLen + bytes, UINT32_MAX));
        for (int i = 0; i < bytes; ++i) {
            EXPECT_EQ(sampleData[dataLen+i], 0xFF);
            sampleData[dataLen+i] = 0x00;
        }
    }
}

TEST_F(CrcTests, Attach32Positive) {
    int bytes = 4;
    for (auto h : h32) {
        EXPECT_TRUE(CRC_attachCRC(&h, sampleData, dataLen + bytes, UINT32_MAX));
        for (int i = 0; i < bytes; ++i) {
            EXPECT_EQ(sampleData[dataLen+i], 0xFF);
            sampleData[dataLen+i] = 0x00;
        }
    }
}

TEST_F(CrcTests, AttachNegative) {
    CRC_Handle_t h;
    EXPECT_FALSE(CRC_attachCRC(NULL, sampleData, dataLen, 0x2));
    EXPECT_FALSE(CRC_attachCRC(&h, NULL, dataLen, 0x2));
    EXPECT_FALSE(CRC_attachCRC(&h, sampleData, 0, 0x2));
}

TEST_F(CrcTests, CheckBlock8Positiv) {
    for (auto h : h8) {
        EXPECT_TRUE(CRC_attachCRC(&h, sampleData, dataLen + h.config.width / 8, CRC_calcBlock(&h, sampleData, dataLen).crc));
        EXPECT_TRUE(CRC_checkBlock(&h, sampleData, dataLen + h.config.width / 8));
    }
}

TEST_F(CrcTests, CheckBlock16Positiv) {
    for (auto h : h16) {
        EXPECT_TRUE(CRC_attachCRC(&h, sampleData, dataLen + h.config.width / 8, CRC_calcBlock(&h, sampleData, dataLen).crc));
        EXPECT_TRUE(CRC_checkBlock(&h, sampleData, dataLen + h.config.width / 8));
    }
}

TEST_F(CrcTests, CheckBlock32Positiv) {
    for (auto h : h32) {
        EXPECT_TRUE(CRC_attachCRC(&h, sampleData, dataLen + h.config.width / 8, CRC_calcBlock(&h, sampleData, dataLen).crc));
        EXPECT_TRUE(CRC_checkBlock(&h, sampleData, dataLen + h.config.width / 8));
    }
}

TEST_F(CrcTests, CheckBlockNegative) {
    CRC_Handle_t h;
    EXPECT_FALSE(CRC_checkBlock(NULL, sampleData, dataLen));
    EXPECT_FALSE(CRC_checkBlock(&h, NULL, dataLen));
    EXPECT_FALSE(CRC_checkBlock(&h, sampleData, 0));
}

TEST_F(CrcTests, CheckByte8Positive) {
    for (auto h : h8) {
        EXPECT_TRUE(CRC_attachCRC(&h, sampleData, dataLen + h.config.width / 8, CRC_calcBlock(&h, sampleData, dataLen).crc));

        for (int i = 0; i < dataLen + h.config.width / 8 - 1; ++i) {
            CRC_checkByte(&h, sampleData[i], false);
        }
        EXPECT_TRUE(CRC_checkByte(&h, sampleData[dataLen + h.config.width / 8 - 1], true));
    }
}

TEST_F(CrcTests, CheckByte16Positive) {
    for (auto h : h16) {
        EXPECT_TRUE(CRC_attachCRC(&h, sampleData, dataLen + h.config.width / 8, CRC_calcBlock(&h, sampleData, dataLen).crc));

        for (int i = 0; i < dataLen + h.config.width / 8 - 1; ++i) {
            CRC_checkByte(&h, sampleData[i], false);
        }
        EXPECT_TRUE(CRC_checkByte(&h, sampleData[dataLen + h.config.width / 8 - 1], true));
    }
}


TEST_F(CrcTests, CheckByte32Positive) {
    for (auto h : h32) {
        EXPECT_TRUE(CRC_attachCRC(&h, sampleData, dataLen + h.config.width / 8, CRC_calcBlock(&h, sampleData, dataLen).crc));

        for (int i = 0; i < dataLen + h.config.width / 8 - 1; ++i) {
            CRC_checkByte(&h, sampleData[i], false);
        }
        EXPECT_TRUE(CRC_checkByte(&h, sampleData[dataLen + h.config.width / 8 - 1], true));
    }
}


TEST_F(CrcTests, CheckByteNegative) {
    EXPECT_FALSE(CRC_checkByte(NULL, 0xFF, true));
}


int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
