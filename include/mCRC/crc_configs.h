#ifndef CRC_CONFIG_H
#define CRC_CONFIG_H

#include "crc.h"

#ifdef __cplusplus
extern "C" {
    namespace mcrc {
#endif

// http://reveng.sourceforge.net/crc-catalogue/

// ############################################## CRC8 ##############################################

// width=8 poly=0x07 init=0x00 refin=false refout=false xorout=0x00 check=0xf4 residue=0x00 name="CRC-8"
CRC_Config_t CRC8_CRC8 = {
    .width = 8,
    .poly = 0x07,
    .init = 0x00,
    .refIn = false,
    .refOut = false,
    .xorOut = 0x00,
    .residue = 0x00
};
// width=8 poly=0x2f init=0xff refin=false refout=false xorout=0xff check=0xdf residue=0x42 name="CRC-8/AUTOSAR"
CRC_Config_t CRC8_AUTOSAR = {
    .width = 8,
    .poly = 0x2F,
    .init = 0xFF,
    .refIn = false,
    .refOut = false,
    .xorOut = 0xFF,
    .residue = 0x42
};

// width=8 poly=0xa7 init=0x00 refin=true refout=true xorout=0x00 check=0x26 residue=0x00 name="CRC-8/BLUETOOTH"
CRC_Config_t CRC8_BLUETOOTH = {
    .width = 8,
    .poly = 0xa7,
    .init = 0x00,
    .refIn = true,
    .refOut = true,
    .xorOut = 0x00,
    .residue = 0x00
};
// width=8 poly=0x39 init=0x00 refin=true refout=true xorout=0x00 check=0x15 residue=0x00 name="CRC-8/DARC"
CRC_Config_t CRC8_DARC = {
    .width = 8,
    .poly = 0x39,
    .init = 0x00,
    .refIn = true,
    .refOut = true,
    .xorOut = 0x00,
    .residue = 0x00
};
// width=8 poly=0x1d init=0xff refin=true refout=true xorout=0x00 check=0x97 residue=0x00 name="CRC-8/EBU"
CRC_Config_t CRC8_EBU = {
    .width = 8,
    .poly = 0x1d,
    .init = 0xff,
    .refIn = true,
    .refOut = true,
    .xorOut = 0x00,
    .residue = 0x00
};
// width=8 poly=0x1d init=0xfd refin=false refout=false xorout=0x00 check=0x7e residue=0x00 name="CRC-8/I-CODE"
CRC_Config_t CRC8_ICODE = {
    .width = 8,
    .poly = 0x1d,
    .init = 0xfd,
    .refIn = false,
    .refOut = false,
    .xorOut = 0x00,
    .residue = 0x00
};
// width=8 poly=0x31 init=0x00 refin=true refout=true xorout=0x00 check=0xa1 residue=0x00 name="CRC-8/MAXIM"
CRC_Config_t CRC8_MAXIM= {
    .width = 8,
    .poly = 0x31,
    .init = 0x00,
    .refIn = true,
    .refOut = true,
    .xorOut = 0x00,
    .residue = 0x00
};
// width=8 poly=0x2f init=0x00 refin=false refout=false xorout=0x00 check=0x3e residue=0x00 name="CRC-8/OPENSAFETY"
CRC_Config_t CRC8_OPENSAFETY = {
    .width = 8,
    .poly = 0x2f,
    .init = 0x00,
    .refIn = false,
    .refOut = false,
    .xorOut = 0x00,
    .residue = 0x00
};
// width=8 poly=0x1d init=0xff refin=false refout=false xorout=0xff check=0x4b residue=0xc4 name="CRC-8/SAE-J1850"
CRC_Config_t CRC8_SAE_J1850 = {
    .width = 8,
    .poly = 0x1d,
    .init = 0xff,
    .refIn = false,
    .refOut = false,
    .xorOut = 0xff,
    .residue = 0xc4
};

// ############################################## CRC16 ##############################################

// width=16 poly=0x8005 init=0x0000 refin=true refout=true xorout=0x0000 check=0xbb3d residue=0x0000 name="CRC-16/ARC"
CRC_Config_t CRC16_ARC = {
    .width = 16,
    .poly = 0x8005,
    .init = 0x0000,
    .refIn = true,
    .refOut = true,
    .xorOut = 0x0000,
    .residue = 0x000
};
// width=16 poly=0x1021 init=0x1d0f refin=false refout=false xorout=0x0000 check=0xe5cc residue=0x0000 name="CRC-16/AUG-CCITT"
CRC_Config_t CRC16_AUG_CCITT = {
    .width = 16,
    .poly = 0x1021,
    .init = 0x1d0f,
    .refIn = false,
    .refOut = false,
    .xorOut = 0x0000,
    .residue = 0x000
};
// width=16 poly=0x8005 init=0x0000 refin=false refout=false xorout=0x0000 check=0xfee8 residue=0x0000 name="CRC-16/BUYPASS"
CRC_Config_t CRC16_BUYPASS = {
    .width = 16,
    .poly = 0x8005,
    .init = 0x0000,
    .refIn = false,
    .refOut = false,
    .xorOut = 0x0000,
    .residue = 0x000
};
// width=16 poly=0x1021 init=0xffff refin=false refout=false xorout=0x0000 check=0x29b1 residue=0x0000 name="CRC-16/CCITT-FALSE"
CRC_Config_t CRC16_CCITT_FALSE = {
    .width = 16,
    .poly = 0x1021,
    .init = 0xffff,
    .refIn = false,
    .refOut = false,
    .xorOut = 0x0000,
    .residue = 0x000
};
// width=16 poly=0x8005 init=0x800d refin=false refout=false xorout=0x0000 check=0x9ecf residue=0x0000 name="CRC-16/DDS-110"
CRC_Config_t CRC16_DDS_110 = {
    .width = 16,
    .poly = 0x8005,
    .init = 0x800d,
    .refIn = false,
    .refOut = false,
    .xorOut = 0x0000,
    .residue = 0x000
};
// width=16 poly=0x0589 init=0x0000 refin=false refout=false xorout=0x0001 check=0x007e residue=0x0589 name="CRC-16/DECT-R"
CRC_Config_t CRC16_DECT_R = {
    .width = 16,
    .poly = 0x0589,
    .init = 0x0000,
    .refIn = false,
    .refOut = false,
    .xorOut = 0x0001,
    .residue = 0x0589
};
// width=16 poly=0x0589 init=0x0000 refin=false refout=false xorout=0x0000 check=0x007f residue=0x0000 name="CRC-16/DECT-X"
CRC_Config_t CRC16_DECT_X = {
    .width = 16,
    .poly = 0x0589,
    .init = 0x0000,
    .refIn = false,
    .refOut = false,
    .xorOut = 0x0000,
    .residue = 0x0000
};
// width=16 poly=0x3d65 init=0x0000 refin=true refout=true xorout=0xffff check=0xea82 residue=0x66c5 name="CRC-16/DNP"
CRC_Config_t CRC16_DNP = {
    .width = 16,
    .poly = 0x3d65,
    .init = 0x0000,
    .refIn = true,
    .refOut = true,
    .xorOut = 0xffff,
    .residue = 0x66c5
};
// width=16 poly=0x3d65 init=0x0000 refin=false refout=false xorout=0xffff check=0xc2b7 residue=0xa366 name="CRC-16/EN-13757"
CRC_Config_t CRC16_EN_13757 = {
    .width = 16,
    .poly = 0x3D65,
    .init = 0x0000,
    .refIn = false,
    .refOut = false,
    .xorOut = 0xffff,
    .residue = 0xA366
};
// width=16 poly=0x1021 init=0xffff refin=false refout=false xorout=0xffff check=0xd64e residue=0x1d0f name="CRC-16/GENIBUS"
CRC_Config_t CRC16_GENIBUS = {
    .width = 16,
    .poly = 0x1021,
    .init = 0xffff,
    .refIn = false,
    .refOut = false,
    .xorOut = 0xffff,
    .residue = 0x1d0f
};
// width=16 poly=0x8005 init=0x0000 refin=true refout=true xorout=0xffff check=0x44c2 residue=0xb001 name="CRC-16/MAXIM"
CRC_Config_t CRC16_MAXIM = {
    .width = 16,
    .poly = 0x8005,
    .init = 0x0000,
    .refIn = true,
    .refOut = true,
    .xorOut = 0xffff,
    .residue = 0xb001
};
// width=16 poly=0x1021 init=0xffff refin=true refout=true xorout=0x0000 check=0x6f91 residue=0x0000 name="CRC-16/MCRF4XX"
CRC_Config_t CRC16_MCRF4XX = {
    .width = 16,
    .poly = 0x1021,
    .init = 0xffff,
    .refIn = true,
    .refOut = true,
    .xorOut = 0x0000,
    .residue = 0x0000
};
// width=16 poly=0x5935 init=0x0000 refin=false refout=false xorout=0x0000 check=0x5d38 residue=0x0000 name="CRC-16/OPENSAFETY-A"
CRC_Config_t CRC16_OPENSAFETY_A = {
    .width = 16,
    .poly = 0x5935,
    .init = 0x0000,
    .refIn = false,
    .refOut = false,
    .xorOut = 0x0000,
    .residue = 0x0000
};
// width=16 poly=0x755b init=0x0000 refin=false refout=false xorout=0x0000 check=0x20fe residue=0x0000 name="CRC-16/OPENSAFETY-B"
CRC_Config_t CRC16_OPENSAFETY_B = {
    .width = 16,
    .poly = 0x755b,
    .init = 0x0000,
    .refIn = false,
    .refOut = false,
    .xorOut = 0x0000,
    .residue = 0x0000
};
// width=16 poly=0x1dcf init=0xffff refin=false refout=false xorout=0xffff check=0xa819 residue=0xe394 name="CRC-16/PROFIBUS"
CRC_Config_t CRC16_PROFIBUS = {
    .width = 16,
    .poly = 0x1dcf,
    .init = 0xffff,
    .refIn = false,
    .refOut = false,
    .xorOut = 0xffff,
    .residue = 0xe394
};
// width=16 poly=0x8bb7 init=0x0000 refin=false refout=false xorout=0x0000 check=0xd0db residue=0x0000 name="CRC-16/T10-DIF"
CRC_Config_t CRC16_T10_DIF = {
    .width = 16,
    .poly = 0x8bb7,
    .init = 0x0000,
    .refIn = false,
    .refOut = false,
    .xorOut = 0x0000,
    .residue = 0x0000
};
// width=16 poly=0xa097 init=0x0000 refin=false refout=false xorout=0x0000 check=0x0fb3 residue=0x0000 name="CRC-16/TELEDISK"
CRC_Config_t CRC16_T10_TELEDISK = {
    .width = 16,
    .poly = 0xa097,
    .init = 0x0000,
    .refIn = false,
    .refOut = false,
    .xorOut = 0x0000,
    .residue = 0x0000
};
// width=16 poly=0x1021 init=0x89ec refin=true refout=true xorout=0x0000 check=0x26b1 residue=0x0000 name="CRC-16/TMS37157"
CRC_Config_t CRC16_TMS37157 = {
    .width = 16,
    .poly = 0x1021,
    .init = 0x89ec,
    .refIn = true,
    .refOut = true,
    .xorOut = 0x0000,
    .residue = 0x0000
};
// width=16 poly=0x1021 init=0xc6c6 refin=true refout=true xorout=0x0000 check=0xbf05 residue=0x0000 name="CRC-A"
CRC_Config_t CRC16_CRC_A = {
    .width = 16,
    .poly = 0x1021,
    .init = 0xc6c6,
    .refIn = true,
    .refOut = true,
    .xorOut = 0x0000,
    .residue = 0x0000
};
// width=16 poly=0x1021 init=0x0000 refin=true refout=true xorout=0x0000 check=0x2189 residue=0x0000 name="CRC-16/CCITT-TRUE"
CRC_Config_t CRC16_CCITT_TRUE = {
    .width = 16,
    .poly = 0x1021,
    .init = 0x0000,
    .refIn = true,
    .refOut = true,
    .xorOut = 0x0000,
    .residue = 0x0000
};
// width=16 poly=0x8005 init=0xffff refin=true refout=true xorout=0x0000 check=0x4b37 residue=0x0000 name="MODBUS"
CRC_Config_t CRC16_MODBUS = {
    .width = 16,
    .poly = 0x8005,
    .init = 0xffff,
    .refIn = true,
    .refOut = true,
    .xorOut = 0x0000,
    .residue = 0x0000
};
// width=16 poly=0x1021 init=0xffff refin=true refout=true xorout=0xffff check=0x906e residue=0xf0b8 name="X-25"
CRC_Config_t CRC16_X_25 = {
    .width = 16,
    .poly = 0x1021,
    .init = 0xFFFF,
    .refIn = true,
    .refOut = true,
    .xorOut = 0xFFFF,
    .residue = 0xF0B8
};
// width=16 poly=0x1021 init=0x0000 refin=false refout=false xorout=0x0000 check=0x31c3 residue=0x0000 name="XMODEM"
CRC_Config_t CRC16_XMODEM = {
    .width = 16,
    .poly = 0x1021,
    .init = 0x0000,
    .refIn = false,
    .refOut = false,
    .xorOut = 0x0000,
    .residue = 0x0000
};

// ############################################## CRC32 ##############################################

// width=32 poly=0x04c11db7 init=0xffffffff refin=true refout=true xorout=0xffffffff check=0xcbf43926 residue=0xdebb20e3 name="CRC-32"
CRC_Config_t CRC32_CRC32 = {
    .width = 32,
    .poly = 0x04C11DB7,
    .init = 0xFFFFFFFF,
    .refIn = true,
    .refOut = true,
    .xorOut = 0xFFFFFFFF,
    .residue = 0xDEBB20E3
};
// width=32 poly=0xf4acfb13 init=0xffffffff refin=true refout=true xorout=0xffffffff check=0x1697d06a residue=0x904cddbf name="CRC-32/AUTOSAR"
CRC_Config_t CRC32_AUTOSAR = {
    .width = 32,
    .poly = 0xF4ACFB13,
    .init = 0xFFFFFFFF,
    .refIn = true,
    .refOut = true,
    .xorOut = 0xFFFFFFFF,
    .residue = 0x904CDDBF
};
// width=32 poly=0x04c11db7 init=0xffffffff refin=false refout=false xorout=0xffffffff check=0xfc891918 residue=0xc704dd7b name="CRC-32/BZIP2"
CRC_Config_t CRC32_BZIP2 = {
    .width = 32,
    .poly = 0x04C11DB7,
    .init = 0xFFFFFFFF,
    .refIn = false,
    .refOut = false,
    .xorOut = 0xFFFFFFFF,
    .residue = 0xC704DD7B
};
// width=32 poly=0x1edc6f41 init=0xffffffff refin=true refout=true xorout=0xffffffff check=0xe3069283 residue=0xb798b438 name="CRC-32C"
CRC_Config_t CRC32_32C = {
    .width = 32,
    .poly = 0x1EDC6F41,
    .init = 0xFFFFFFFF,
    .refIn = true,
    .refOut = true,
    .xorOut = 0xFFFFFFFF,
    .residue = 0xB798B438
};
// width=32 poly=0xa833982b init=0xffffffff refin=true refout=true xorout=0xffffffff check=0x87315576 residue=0x45270551 name="CRC-32D"
CRC_Config_t CRC32_32D = {
    .width = 32,
    .poly = 0xA833982B,
    .init = 0xFFFFFFFF,
    .refIn = true,
    .refOut = true,
    .xorOut = 0xFFFFFFFF,
    .residue = 0x45270551
};
// width=32 poly=0x04c11db7 init=0xffffffff refin=false refout=false xorout=0x00000000 check=0x0376e6e7 residue=0x00000000 name="CRC-32/MPEG-2"
CRC_Config_t CRC32_MPEG_2 = {
    .width = 32,
    .poly = 0x04C11DB7,
    .init = 0xFFFFFFFF,
    .refIn = false,
    .refOut = false,
    .xorOut = 0x00000000,
    .residue = 0x00000000
};
// width=32 poly=0x04c11db7 init=0x00000000 refin=false refout=false xorout=0xffffffff check=0x765e7680 residue=0xc704dd7b name="CRC-32/POSIX"
CRC_Config_t CRC32_POSIX = {
    .width = 32,
    .poly = 0x04C11DB7,
    .init = 0x00000000,
    .refIn = false,
    .refOut = false,
    .xorOut = 0xFFFFFFFF,
    .residue = 0xC704DD7B
};
// width=32 poly=0x814141ab init=0x00000000 refin=false refout=false xorout=0x00000000 check=0x3010bf7f residue=0x00000000 name="CRC-32Q"
CRC_Config_t CRC32_32Q = {
    .width = 32,
    .poly = 0x814141AB,
    .init = 0x00000000,
    .refIn = false,
    .refOut = false,
    .xorOut = 0x00000000,
    .residue = 0x00000000
};
// width=32 poly=0x04c11db7 init=0xffffffff refin=true refout=true xorout=0x00000000 check=0x340bc6d9 residue=0x00000000 name="JAMCRC"
CRC_Config_t CRC32_JAMCRC = {
    .width = 32,
    .poly = 0x04C11DB7,
    .init = 0xFFFFFFFF,
    .refIn = true,
    .refOut = true,
    .xorOut = 0x00000000,
    .residue = 0x00000000
};
// width=32 poly=0x000000af init=0x00000000 refin=false refout=false xorout=0x00000000 check=0xbd0be338 residue=0x00000000 name="XFER"
CRC_Config_t CRC32_XFER = {
    .width = 32,
    .poly = 0x000000AF,
    .init = 0x00000000,
    .refIn = false,
    .refOut = false,
    .xorOut = 0x00000000,
    .residue = 0x00000000
};

#ifdef __cplusplus
    } // namespace mcrc
} // extern "C"
#endif

#endif // CRC_CONFIG_H
