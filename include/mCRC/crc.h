#ifndef CRC_H
#define CRC_H

#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    uint32_t width;
    uint32_t poly;
    uint32_t init;
    uint32_t xorOut;
    uint32_t residue;
    bool refIn;
    bool refOut;
} CRC_Config_t;

typedef struct {
    CRC_Config_t config;
    uint32_t _reg;
    uint32_t _LUT[256];
} CRC_Handle_t;

typedef struct {
    uint32_t crc;
    bool isValid;
} CRC_Result_t;

bool CRC_init(CRC_Handle_t* h);

CRC_Result_t CRC_calcBlock(CRC_Handle_t* h, uint8_t* data, uint32_t size);
CRC_Result_t CRC_calcByte(CRC_Handle_t* h, uint8_t data, bool isLast);

bool CRC_checkBlock(CRC_Handle_t* h, uint8_t* data, uint32_t size);
bool CRC_checkByte(CRC_Handle_t* h, uint8_t data, bool isLast);
bool CRC_attachCRC(CRC_Handle_t* h, uint8_t* data, uint32_t size, uint32_t crc);

#ifdef __cplusplus
}
#endif

#endif
