#include "crc.h"

namespace mcrc {

using result = CRC_Result_t;
using config = CRC_Config_t;

class Crc {
public:
    Crc(config& cfg) noexcept(true) {
        h.config = cfg;
        (void)CRC_init(&h);
    }
    inline result calc(uint8_t* data, uint32_t size) noexcept(true) {
        return CRC_calcBlock(&h, data, size);
    }
    inline result calc(uint8_t data, bool isLast) noexcept(true) {
        return CRC_calcByte(&h, data, isLast);
    }
    inline bool check(uint8_t* data, uint32_t size) noexcept(true) {
        return CRC_checkBlock(&h, data, size);
    }
    inline bool check(uint8_t data, bool isLast) noexcept(true) {
        return CRC_checkByte(&h, data, isLast);
    }
    inline bool attach(uint8_t* data, uint32_t size, uint32_t crc) noexcept(true) {
        return CRC_attachCRC(&h, data, size, crc);
    }
    inline const config get() const noexcept(true) {
        return h.config;
    }

private:
    CRC_Handle_t h;
};

}  // namespace mcrc
