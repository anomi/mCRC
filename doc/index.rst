.. myCoolProject documentation master file, created by
   sphinx-quickstart on Sun Apr 22 21:16:04 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to myCoolProject's documentation!
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. c:function:: PyObject* PyType_GenericAlloc(PyTypeObject *type, Py_ssize_t nitems)
.. cpp:member:: std::string MyClass::myMember

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
