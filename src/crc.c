#include "crc.h"

static inline uint32_t reflectBits(uint32_t v, uint8_t nBit);
static inline uint32_t genResidue(CRC_Handle_t* h);
static inline bool isPowerOfTwo(uint32_t v);


static const uint8_t ByteReversLUT[256] = {
#   define R2(n)     (n),     (n) + 2*64,     (n) + 1*64,     (n) + 3*64
#   define R4(n) R2(n), R2((n) + 2*16), R2((n) + 1*16), R2((n) + 3*16)
#   define R6(n) R4(n), R4((n) + 2*4 ), R4((n) + 1*4 ), R4((n) + 3*4 )
    R6(0), R6(2), R6(1), R6(3)
};

bool CRC_init(CRC_Handle_t* const h) {
    if (!h
        || (h->config.width < 8)
        || (h->config.width > 32)
        || !isPowerOfTwo(h->config.width)
        || (h->config.refIn != h->config.refOut)
        || (h->config.poly & ((uint64_t)UINT32_MAX << (h->config.width)))
        || (h->config.init & ((uint64_t)UINT32_MAX << (h->config.width)))
        || (h->config.xorOut & ((uint64_t)UINT32_MAX << (h->config.width)))
        ) {
        return false;
    }

    const uint32_t width = h->config.width;
    const uint32_t mask = (UINT32_MAX >> (32 - width));
    const uint32_t poly = h->config.poly;
    uint32_t reg = 0;
    h->_reg = h->config.init;

    // generate lookup table
    for (uint32_t crcByte = 0; crcByte < 256; ++crcByte) {
        reg = (crcByte << (width - 8));

        for (uint32_t bit = 0; bit < 8; ++bit) {
            if ((reg & (1U << (width - 1U))) != 0) {
                reg = ((reg << 1U) ^ poly) & mask;
            } else {
                reg = (reg << 1U) & mask;
            }
        }

        h->_LUT[crcByte] = reg;
    }

    if((h->config.xorOut != 0 && h->config.residue == 0)) {
        h->config.residue = genResidue(h);
    }

    return true;
}

CRC_Result_t CRC_calcBlock(CRC_Handle_t* const h,
                           uint8_t* const data,
                           const uint32_t size) {
    CRC_Result_t r;
    r.isValid = false;
    r.crc = 0;
    uint32_t n = 0;

    // reduced set of checks
    if (!h
        || !data
        || size == 0) {
        return r;
    }

    for (n = 0; n < size - 1; ++n) {
        CRC_calcByte(h, data[n], false);
    }

    return CRC_calcByte(h, data[n], true);
}

CRC_Result_t CRC_calcByte(CRC_Handle_t* const h,
                          uint8_t data,
                          const bool isLast) {
    CRC_Result_t r;
    r.isValid = false;
    r.crc = 0;

    // reduced checks
    if (!h) {
        return r;
    }

    uint8_t pos = 0;
    const uint32_t width = h->config.width;
    const uint32_t mask = (UINT32_MAX >> (32 - width));
    const uint32_t xorOut = h->config.xorOut;
    const bool refIn = h->config.refIn;
    const bool refOut = h->config.refOut;

    if (refIn == true) {
        data = reflectBits(data, 8);
    }

    pos = (h->_reg >> (width - 8)) ^ data;

    if (width <= 8) {
        h->_reg = h->_LUT[pos] & mask;
    }

    if (width > 8) {
        h->_reg = ((h->_reg << 8U) ^ h->_LUT[pos]) & mask;
    }

    if (isLast == true) {
        r.isValid = true;

        if (refOut == true) {
            r.crc = reflectBits(h->_reg, width) ^ xorOut;
        } else                {
            r.crc = h->_reg ^ xorOut;
        }

        h->_reg = h->config.init;
    }

    return r;
}

bool CRC_checkBlock(CRC_Handle_t* h,
                    uint8_t* data,
                    uint32_t size) {
    if (!h
        || !data
        || size == 0) {
        return false;
    }

    uint32_t n = 0;

    for (n = 0; n < size - 1; ++n) {
        CRC_checkByte(h, data[n], false);
    }

    return CRC_checkByte(h, data[n], true);
}

bool CRC_checkByte(CRC_Handle_t* const h,
                   const uint8_t data,
                   const bool isLast) {
    if (!h) {
        return false;
    }

    const uint32_t width = h->config.width;
    const uint32_t mask = (UINT32_MAX >> (32 - width));
    const uint32_t xorOut = h->config.xorOut;
    CRC_Result_t res;
    bool r = false;
    res = CRC_calcByte(h, data, isLast);

    if (isLast == true) {
        if (h->config.residue != 0) {
            r = ((res.crc ^ xorOut) & mask) == h->config.residue ? true : false;
        } else {
            r = res.crc == 0 ? true : false;
        }
        h->_reg = h->config.init;
    }

    return r;
}

bool CRC_attachCRC(CRC_Handle_t* const h,
                   uint8_t* data,
                   const uint32_t size,
                   const uint32_t crc) {
    uint32_t i = 0;
    if (!h
        || !data
        || size == 0) {
        return false;
    }

    const uint32_t width = h->config.width / 8;
    if (h->config.refIn
        && h->config.refOut) {
        for (i = 0; i < width; ++i) {
            data[size - (width - i)] = (crc & (0xFFU << 8U * i)) >> (8U * i);
        }
    } else {
        for (i = 0; i < width; ++i) {
            data[size - (width - i)] = (crc & (0xFFU << 8U * (width - 1U - i))) >> (8U * (width - 1U - i));
        }
    }

    return true;
}

static inline uint32_t genResidue(CRC_Handle_t* const h) {
    const uint32_t width = h->config.width;
    const uint32_t dataLen = 9;
    const uint32_t crcLen = (width / 8);
    const uint32_t mask = (UINT32_MAX >> (32 - width));
    const uint32_t xorOut = h->config.xorOut;

    uint8_t sampleData[] = {0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x00, 0x00, 0x00, 0x00};
    uint32_t residue = 0;
    uint32_t crc = CRC_calcBlock(h, sampleData, dataLen).crc;
    CRC_attachCRC(h, sampleData, dataLen + crcLen, crc);
    residue = (CRC_calcBlock(h, sampleData, crcLen + dataLen).crc ^ xorOut) & mask;
    return residue;
}

static inline bool isPowerOfTwo(uint32_t v) {
    return (v - 1) & v ? false : true;
}

static inline uint32_t reflectBits(const uint32_t v, const uint8_t nBit) {
    uint32_t rv = 0;
    uint32_t i = 0;

    for (i = 0; i < (nBit / 8); ++i) {
        rv |= ByteReversLUT[(v & (0xFFU << (8U * i))) >> 8U * i] << (nBit - 8U * (i + 1U));
    }

    return rv;
}
